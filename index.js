/* var processorsArray = [
  require('autoprefixer')({ grid: true, browsers: ['last 2 versions', 'ie 6-8', 'Firefox > 20']  })
];*/

window.onscroll = () => {
  scroll()
};

let scroll = () => {
  let navbar = document.getElementById('js-navbar');
  let sticky = navbar.offsetTop;

  if (window.pageYOffset <= 200) {
    navbar.classList.add("sticky");
  } else {
    navbar.classList.remove("sticky");
  }
};
